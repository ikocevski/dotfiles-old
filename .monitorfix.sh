#!bin/bash/

pkill -9 compton &&
	xrandr --newmode "1366x768_59.79"   85.50  1366 1440 1576 1784  768 771 781 798 +hsync +vsync &&
	xrandr --addmode DVI-I-1 1366x768_59.79 &&
	xrandr --output DVI-I-1 --mode 1366x768_59.79 &&
	nohup compton -f -D 5 --backend glx --vsync opengl-mswc &
